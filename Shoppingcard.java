package book.lk;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Shoppingcard extends JFrame {

	private JPanel contentPane;
	private JTextField cid;
	private JTextField uid;
	private JTextField bid;
	private JTextField date;
	private JTextField quality;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Shoppingcard frame = new Shoppingcard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Shoppingcard() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 576, 375);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(107, 142, 35));
		contentPane.setBorder(new EmptyBorder(6, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCardId = new JLabel("Card Id");
		lblCardId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCardId.setBounds(41, 32, 60, 14);
		contentPane.add(lblCardId);
		
		JLabel lblUserId = new JLabel("User Id");
		lblUserId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUserId.setBounds(41, 64, 60, 14);
		contentPane.add(lblUserId);
		
		JLabel lblBookId = new JLabel("Book Id");
		lblBookId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBookId.setBounds(41, 101, 60, 14);
		contentPane.add(lblBookId);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDate.setBounds(41, 132, 46, 14);
		contentPane.add(lblDate);
		
		JLabel lblQuality = new JLabel("Quality");
		lblQuality.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblQuality.setBounds(41, 163, 60, 14);
		contentPane.add(lblQuality);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
			
		});
		btnOk.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnOk.setBounds(41, 217, 89, 23);
		contentPane.add(btnOk);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnCancel.setBounds(202, 217, 89, 23);
		contentPane.add(btnCancel);
		
		cid = new JTextField();
		cid.setBounds(202, 31, 86, 20);
		contentPane.add(cid);
		cid.setColumns(10);
		
		uid = new JTextField();
		uid.setBounds(202, 63, 86, 20);
		contentPane.add(uid);
		uid.setColumns(10);
		
		bid = new JTextField();
		bid.setBounds(202, 100, 86, 20);
		contentPane.add(bid);
		bid.setColumns(10);
		
		date = new JTextField();
		date.setBounds(202, 131, 86, 20);
		contentPane.add(date);
		date.setColumns(10);
		
		quality = new JTextField();
		quality.setBounds(202, 162, 86, 20);
		contentPane.add(quality);
		quality.setColumns(10);
	}
}
