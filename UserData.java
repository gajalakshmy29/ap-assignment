package book.lk;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class UserData extends BookData {

	private JPanel contentPane;
	private JTextField userid;
	private JTextField name;
	private JTextField pass;
	private JTextField Email;
	private JTextField Address;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserData frame = new UserData();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserData() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 153, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUserId = new JLabel("User ID");
		lblUserId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUserId.setBounds(63, 44, 64, 14);
		contentPane.add(lblUserId);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblName.setBounds(63, 82, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(63, 117, 75, 14);
		contentPane.add(lblPassword);
		
		JLabel lblAddress = new JLabel("Email");
		lblAddress.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddress.setBounds(63, 153, 46, 14);
		contentPane.add(lblAddress);
		
		userid = new JTextField();
		userid.setBounds(225, 43, 86, 20);
		contentPane.add(userid);
		userid.setColumns(10);
		
		name = new JTextField();
		name.setBounds(225, 81, 86, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		pass = new JTextField();
		pass.setBounds(225, 114, 86, 20);
		contentPane.add(pass);
		pass.setColumns(10);
		
		Email = new JTextField();
		Email.setBounds(225, 150, 86, 20);
		contentPane.add(Email);
		Email.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
	                //Creating Connection Object
	                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/library","root","root");
	                //Prepared Statement
	                PreparedStatement Pstatement=con.prepareStatement ("insert into login (id,name,password,Email,Address) values(?,?,?,?,?)");

	                		
	                //Specifying the values of it's parameter
	                Pstatement.setString(1,userid.getText());
	                Pstatement.setString(2,name.getText());
	                Pstatement.setString(3,pass.getText());
	                Pstatement.setString(4,Email.getText());
	                Pstatement.setString(5,Address.getText());
	                
	                //Checking for the Password match
	               // if(passwordField.getText().equalsIgnoreCase(confirmPasswordField.getText()))
	               // {
	                    //Executing query
	                    Pstatement.executeUpdate();
	                    JOptionPane.showMessageDialog(null,"Data Registered Successfully");
	                
	                
	                
	 
	            } catch (SQLException e1) {
	                e1.printStackTrace();
	            }
				
			}
		});
		btnOk.setBounds(54, 227, 89, 23);
		contentPane.add(btnOk);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnCancel.setBounds(225, 227, 89, 23);
		contentPane.add(btnCancel);
		
		JLabel lblAddress_1 = new JLabel("Address");
		lblAddress_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddress_1.setBounds(63, 190, 64, 14);
		contentPane.add(lblAddress_1);
		
		Address = new JTextField();
		Address.setBounds(225, 184, 86, 20);
		contentPane.add(Address);
		Address.setColumns(10);
	}

}
