package book.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class Loginpage extends JFrame {

	private JPanel contentPane;
	private JTextField uname;
	private JPasswordField upass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Loginpage frame = new Loginpage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Loginpage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 456, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 139, 139));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblUserName.setBounds(46, 40, 82, 32);
		contentPane.add(lblUserName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPassword.setBounds(46, 83, 91, 35);
		contentPane.add(lblPassword);
		
		uname = new JTextField();
		uname.setBounds(198, 42, 105, 23);
		contentPane.add(uname);
		uname.setColumns(10);
		
		upass = new JPasswordField();
		upass.setBounds(198, 86, 105, 23);
		contentPane.add(upass);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBackground(new Color(0, 0, 0));
		btnLogin.setForeground(new Color(0, 255, 0));
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username =uname.getText();
				String userpassword =upass.getText();
				 try{
				    	Class.forName("com.mysql.jdbc.Driver");
				           Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
				           
				         
				           String query=("select name,password from login");
				           PreparedStatement pst= con.prepareStatement(query);
				           ResultSet rs=pst.executeQuery();
				           while(rs.next()){
				              
				        	   String dbname=rs.getString(1);
				        	   String dbpass=rs.getString(2);
				        	   
				           
				          if(username.equals(dbname) && userpassword.equals(dbpass)) {
				        	  Home jframe = new Home();
					          jframe.setVisible(true);
				         // JOptionPane.showMessageDialog(null," login success");
				        
				          }
				          else{
				          
				         

				          }
				          
				           }
				    
			           } catch(Exception e1){
			
				 
			}
			}
		});
		btnLogin.setBounds(46, 153, 89, 23);
		contentPane.add(btnLogin);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBackground(new Color(0, 0, 0));
		btnCancel.setForeground(new Color(255, 0, 0));
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnCancel.setBounds(198, 153, 89, 23);
		contentPane.add(btnCancel);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setBackground(new Color(0, 0, 0));
		btnRegister.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UserData jframe = new UserData();
		          jframe.setVisible(true);
			}
		});
		btnRegister.setBounds(103, 203, 89, 23);
		contentPane.add(btnRegister);
		
		JButton btnNewButton = new JButton("Clear");
		btnNewButton.setBackground(new Color(0, 0, 0));
		btnNewButton.setForeground(new Color(65, 105, 225));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				uname.setText(null);
				upass.setText(null);
			}
		});
		btnNewButton.setBounds(256, 203, 89, 23);
		contentPane.add(btnNewButton);
	}
}
