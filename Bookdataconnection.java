package book.lk;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Bookdataconnection {
	
		public static int add(int bookid, String title, String author, int isbn, float price
				) {
			Connection conn = null;

			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");
			} catch (Exception e) {
				{
					System.out.println(e);
				}

			}
			int status = 0;
			try {

				PreparedStatement ps = conn.prepareStatement(
						"insert into bookdata(bookid,title,author,isbn,price) values(?,?,?,?,?)");

				ps.setInt(1, bookid);

				ps.setString(2, title);

				ps.setString(3, author);

				ps.setInt(4,isbn);

				ps.setFloat(5, price);

				

				
				status = ps.executeUpdate();
				conn.close();
			} catch (Exception e) {
				System.out.println(e);
			}
			return status;
		}
	}


