package book.lk;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Book_add extends JFrame {

	private JPanel contentPane;
	private JTextField book_id;
	private JTextField te;
	private JTextField ar;
	private JTextField in;
	private JTextField pz;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book_add frame = new Book_add();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	Connection conn = null;
	/**
	 * Create the frame.
	 */
	public Book_add() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "root");{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 329, 362);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBackground(new Color(0, 51, 51));
		contentPane.add(panel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("Book ID");
		label.setForeground(new Color(204, 0, 0));
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(47, 44, 79, 14);
		panel.add(label);
		
		JLabel label_1 = new JLabel("Title");
		label_1.setForeground(new Color(204, 0, 0));
		label_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_1.setBounds(47, 82, 46, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Author");
		label_2.setForeground(new Color(204, 0, 0));
		label_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_2.setBounds(47, 118, 57, 14);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("ISBN");
		label_3.setForeground(new Color(204, 0, 0));
		label_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_3.setBounds(47, 161, 46, 14);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("Price");
		label_4.setForeground(new Color(204, 0, 0));
		label_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_4.setBounds(46, 201, 68, 20);
		panel.add(label_4);
		
		book_id = new JTextField();
		book_id.setColumns(10);
		book_id.setBounds(163, 41, 86, 20);
		panel.add(book_id);
		
		te = new JTextField();
		te.setColumns(10);
		te.setBounds(163, 81, 86, 20);
		panel.add(te);
		
		ar = new JTextField();
		ar.setColumns(10);
		ar.setBounds(163, 117, 86, 20);
		panel.add(ar);
		
		in = new JTextField();
		in.setColumns(10);
		in.setBounds(163, 160, 86, 20);
		panel.add(in);
		
		pz = new JTextField();
		pz.setColumns(10);
		pz.setBounds(163, 203, 86, 20);
		panel.add(pz);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int bookid=Integer.parseInt(book_id.getText());
				String title=te.getText();
				String author=ar.getText() ;
				int isbn=Integer.parseInt(in.getText());
				float price=Float.parseFloat(pz.getText());
				
				 int i=Bookdataconnection.add(bookid,title, author,isbn, price);
					if(i>0) {
						JOptionPane.showMessageDialog(Book_add.this, "Add Book Successfully");
						
						book_id.setText(null);
						te.setText(null);
						ar.setText(null);
						in.setText(null);
						pz.setText(null);
						
						
					}else{
						JOptionPane.showMessageDialog(Book_add.this,"Sorry, unable to Add!");
					}
				
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnNewButton.setBounds(37, 253, 89, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnNewButton_1.setBounds(160, 253, 89, 23);
		panel.add(btnNewButton_1);
	}
	}
}
