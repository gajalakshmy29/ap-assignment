-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.15-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for library
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library`;


-- Dumping structure for table library.book purcharse
CREATE TABLE IF NOT EXISTS `book purcharse` (
  `User ID` int(11) DEFAULT NULL,
  `Book ID` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Total amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table library.book purcharse: ~3 rows (approximately)
/*!40000 ALTER TABLE `book purcharse` DISABLE KEYS */;
INSERT INTO `book purcharse` (`User ID`, `Book ID`, `Date`, `Total amount`) VALUES
	(1, 1, '2019-05-18', 2500),
	(2, 2, '2019-05-19', 3000),
	(3, 5, '2019-05-21', 1000);
/*!40000 ALTER TABLE `book purcharse` ENABLE KEYS */;


-- Dumping structure for table library.bookdata
CREATE TABLE IF NOT EXISTS `bookdata` (
  `bookid` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `isbn` int(11) unsigned zerofill DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`bookid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table library.bookdata: ~3 rows (approximately)
/*!40000 ALTER TABLE `bookdata` DISABLE KEYS */;
INSERT INTO `bookdata` (`bookid`, `title`, `author`, `isbn`, `price`) VALUES
	(1, 'Technology', 'Sivakumar', 00000000001, 2500),
	(2, 'Basic technology', 'KUKAN', 00000000002, 3000),
	(3, 'Harry potter', 'wicken', 00000000200, 1000);
/*!40000 ALTER TABLE `bookdata` ENABLE KEYS */;


-- Dumping structure for table library.login
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table library.login: ~7 rows (approximately)
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`id`, `name`, `password`, `Email`, `Address`) VALUES
	(1, 'vithu', 'vithu', 'vithu@gmail.com', 'colombo'),
	(2, 'sharmila', 'sharmila', 'sharmila@gmail.com', 'jaffna'),
	(3, 'barmila', 'barmila', 'barmila@gmail.com', 'mannar'),
	(4, 'gaju', 'gaju', 'gaju@gmail.com', 'london'),
	(5, 'inthu', 'inthu', 'inthu@gmail.com', 'london'),
	(6, 'lakshmy', 'lakshmy', 'lakshmy@gmail.com', 'london'),
	(7, 'kiruthi', 'kiruthi', 'kiruthi@gmail.com', 'colombo');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


-- Dumping structure for table library.shopping  card
CREATE TABLE IF NOT EXISTS `shopping  card` (
  `Card ID` int(11) DEFAULT NULL,
  `User ID` int(11) DEFAULT NULL,
  `Book ID` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Quality` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table library.shopping  card: ~3 rows (approximately)
/*!40000 ALTER TABLE `shopping  card` DISABLE KEYS */;
INSERT INTO `shopping  card` (`Card ID`, `User ID`, `Book ID`, `Date`, `Quality`) VALUES
	(1212, 1, 1, '2019-05-18', 'High'),
	(1414, 2, 2, '2019-05-19', 'standard'),
	(1616, 3, 3, '2019-05-20', 'low');
/*!40000 ALTER TABLE `shopping  card` ENABLE KEYS */;


-- Dumping structure for table library.user data
CREATE TABLE IF NOT EXISTS `user data` (
  `User ID` int(11) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Password` int(11) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table library.user data: ~0 rows (approximately)
/*!40000 ALTER TABLE `user data` DISABLE KEYS */;
/*!40000 ALTER TABLE `user data` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
