package book.lk;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class BookData extends BookPurchers {

	private JPanel contentPane;
	private JTextField bookid;
	private JTextField title;
	private JTextField author;
	private JTextField isbn;
	private JTextField price;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookData frame = new BookData();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BookData() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 378);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 51, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBookId = new JLabel("Book ID");
		lblBookId.setForeground(new Color(204, 0, 0));
		lblBookId.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblBookId.setBounds(47, 44, 79, 14);
		contentPane.add(lblBookId);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setForeground(new Color(204, 0, 0));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTitle.setBounds(47, 82, 46, 14);
		contentPane.add(lblTitle);
		
		JLabel lblNewLabel = new JLabel("Author");
		lblNewLabel.setForeground(new Color(204, 0, 0));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(47, 118, 57, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("ISBN");
		lblNewLabel_1.setForeground(new Color(204, 0, 0));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(47, 161, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setForeground(new Color(204, 0, 0));
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPrice.setBounds(46, 201, 68, 20);
		contentPane.add(lblPrice);
		
		bookid = new JTextField();
		bookid.setBounds(163, 41, 86, 20);
		contentPane.add(bookid);
		bookid.setColumns(10);
		
		title = new JTextField();
		title.setBounds(163, 81, 86, 20);
		contentPane.add(title);
		title.setColumns(10);
		
		author = new JTextField();
		author.setBounds(163, 117, 86, 20);
		contentPane.add(author);
		author.setColumns(10);
		
		isbn = new JTextField();
		isbn.setBounds(163, 160, 86, 20);
		contentPane.add(isbn);
		isbn.setColumns(10);
		
		price = new JTextField();
		price.setBounds(163, 203, 86, 20);
		contentPane.add(price);
		price.setColumns(10);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
	                //Creating Connection Object
	                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/library","root","root");
	                //Prepared Statement
	                PreparedStatement Pstatement=con.prepareStatement ("insert into book data(bookid,title,author,isbn,price) values(?,?,?,?,?)");

	                		
	                //Specifying the values of it's parameter
	                Pstatement.setString(1,bookid.getText());
	                Pstatement.setString(2,title.getText());
	                Pstatement.setString(3,author.getText());
	                Pstatement.setString(4,isbn.getText());
	                Pstatement.setString(5,price.getText());
	                
	                //Checking for the Password match
	               // if(passwordField.getText().equalsIgnoreCase(confirmPasswordField.getText()))
	               // {
	                    //Executing query
	                    Pstatement.executeUpdate();
	                    JOptionPane.showMessageDialog(null,"bookdata Registered Successfully");
	                
	                
	                
	 
	            } catch (SQLException e1) {
	                e1.printStackTrace();
	            }
				
			}
			
		});
		btnNewButton.setBounds(37, 253, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnNewButton_1.setBounds(160, 253, 89, 23);
		contentPane.add(btnNewButton_1);
	}

}
